package com.novruz.commonsecurity.auth;

import com.novruz.commonsecurity.configuration.SecurityProperties;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import java.security.Key;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public final class JwtService {

    private final SecurityProperties securityProperties;
    private Key key;

    @PostConstruct
    public void init() {
        byte[] keyBytes;
        keyBytes = Decoders.BASE64.decode(securityProperties.getJwtProperties().getSecret());
        key = Keys.hmacShaKeyFor(keyBytes);
    }

    public Claims parseToken(String token) {
        Claims body = Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
        return body;
    }

    public String issueToken(Authentication authentication) {
        final JwtBuilder jwtBuilder = Jwts.builder()
                .setSubject(authentication.getName())
                .addClaims(Map.of("ROLE", List.of("ADMIN", "USER")))
                .setIssuedAt(new Date())
                .setExpiration(
                        Date.from(Instant.now().plus(Duration
                                .ofSeconds(securityProperties.getJwtProperties().getValidityInSeconds()))))
                .setHeader(Map.of("type", "JWT"))
                .signWith(key, SignatureAlgorithm.HS512);

        return jwtBuilder.compact();
    }
}
