package com.novruz.usermanagementms.repository;

import com.novruz.usermanagementms.domain.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {
}
