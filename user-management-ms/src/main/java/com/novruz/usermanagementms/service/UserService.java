package com.novruz.usermanagementms.service;

import com.novruz.usermanagementms.domain.SignUpRequest;

public interface UserService {
    void signUp(SignUpRequest signUpRequest);
}
