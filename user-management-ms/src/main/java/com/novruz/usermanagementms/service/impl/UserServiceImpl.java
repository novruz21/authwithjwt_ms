package com.novruz.usermanagementms.service.impl;

import com.novruz.usermanagementms.domain.Authority;
import com.novruz.usermanagementms.domain.SignUpRequest;
import com.novruz.usermanagementms.domain.User;
import com.novruz.usermanagementms.repository.UserRepository;
import com.novruz.usermanagementms.service.UserService;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public void signUp(SignUpRequest signUpRequest) {
        userRepository.findByUsername(signUpRequest.getEmail())
                .ifPresent(user ->
                {
                    throw new RuntimeException("Email is already present");
                });

        User user = new User();
        user.setPassword(passwordEncoder.encode(signUpRequest.getPassword()));
        user.setAuthorities(new ArrayList<>());
        user.setAccountNonExpired(true);
        user.setAccountNonLocked(true);
        user.setCredentialsNonExpired(true);
        user.setEnabled(true);
        user.setUsername(signUpRequest.getEmail());

        Authority authority = new Authority();
        authority.setAuthority("ROLE_admin");
        user.setAuthorities(List.of(authority));

        userRepository.save(user);
    }
}
