package com.novruz.usermanagementms.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = "password")
public class SignUpRequest {
    private String name;
    private String email;
    private String password;
}
