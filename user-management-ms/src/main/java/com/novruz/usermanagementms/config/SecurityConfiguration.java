package com.novruz.usermanagementms.config;

import com.novruz.commonsecurity.configuration.BaseSecurityConfig;
import com.novruz.commonsecurity.filter.FilterConfigurerAdapter;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;

import static org.springframework.security.config.Customizer.withDefaults;

@EnableWebSecurity
@ComponentScan("com.novruz.commonsecurity")
public class SecurityConfiguration extends BaseSecurityConfig {

    public SecurityConfiguration(FilterConfigurerAdapter filterConfigurerAdapter) {
        super(filterConfigurerAdapter);
    }

    @Override
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests((authz) -> authz.antMatchers("/auth/sign-in").permitAll()
                                                        .antMatchers("/auth/sign-up").permitAll())
                .httpBasic(withDefaults());
        return super.filterChain(http);
    }
}
