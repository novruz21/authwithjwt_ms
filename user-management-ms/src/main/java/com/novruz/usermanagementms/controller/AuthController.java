package com.novruz.usermanagementms.controller;

import com.novruz.commonsecurity.auth.JwtService;
import com.novruz.usermanagementms.domain.AccessTokenResponse;
import com.novruz.usermanagementms.domain.LoginRequest;
import com.novruz.usermanagementms.domain.SignUpRequest;
import com.novruz.usermanagementms.service.UserService;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("auth")
public class AuthController {

    private final JwtService jwtService;
    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final UserService userService;

    @PostMapping("sign-in")
    public ResponseEntity<AccessTokenResponse> authorize(@Valid @RequestBody LoginRequest loginDto) {
        Authentication authenticationToken = new UsernamePasswordAuthenticationToken(loginDto.getEmail(),
                loginDto.getPassword());

        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = jwtService.issueToken(authentication);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.AUTHORIZATION, "Bearer " + jwt);
        return new ResponseEntity<>(new AccessTokenResponse(jwt), httpHeaders, HttpStatus.OK);
    }

    @PostMapping("sign-up")
    public ResponseEntity<Boolean> signUp(@RequestBody SignUpRequest signUpRequest) {
        userService.signUp(signUpRequest);
        return ResponseEntity.ok(true);
    }
}
