package com.novruz.libraryms.dto;

import com.novruz.libraryms.domain.Author;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = "author")
public class BookDto {
    private String title;
    private String isbnNumber;
    private Author author;
}
