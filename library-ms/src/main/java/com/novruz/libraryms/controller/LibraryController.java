package com.novruz.libraryms.controller;

import com.novruz.libraryms.dto.BookDto;
import com.novruz.libraryms.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("library")
public class LibraryController {

    private final BookService bookService;

    @GetMapping("/public") //no auth
    public String sayHelloPublic() {
        return "Hello Everyone";
    }

    @GetMapping("books")
    public ResponseEntity<Page<BookDto>> getBooks(Pageable pageable) {
        return ResponseEntity.ok(bookService.getAllBooks(pageable));
    }

    @PostMapping("book")
    public ResponseEntity<BookDto> publishBook(@RequestBody BookDto bookDto) {
        return ResponseEntity.ok(bookService.addBook(bookDto));
    }
}
