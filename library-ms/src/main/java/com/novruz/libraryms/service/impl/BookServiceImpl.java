package com.novruz.libraryms.service.impl;

import com.novruz.libraryms.domain.Book;
import com.novruz.libraryms.dto.BookDto;
import com.novruz.libraryms.repository.BookRepository;
import com.novruz.libraryms.service.BookService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;
    private final ModelMapper modelMapper;

    @Override
    public Page<BookDto> getAllBooks(Pageable pageable) {
        var books = bookRepository.findAll(pageable);
        log.info("Returned books: {}", books);
        return books.map(book -> {
            return modelMapper.map(book, BookDto.class);
        });
    }

    @Override
    public BookDto addBook(BookDto bookDto) {
        var book = bookRepository.save(modelMapper.map(bookDto, Book.class));
        return modelMapper.map(book, BookDto.class);
    }
}
