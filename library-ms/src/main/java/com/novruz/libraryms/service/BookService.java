package com.novruz.libraryms.service;

import com.novruz.libraryms.dto.BookDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface BookService {
    Page<BookDto> getAllBooks(Pageable pageable);

    BookDto addBook(BookDto bookDto);
}
