package com.novruz.libraryms.configuration;

import com.novruz.commonsecurity.auth.JwtService;
import com.novruz.commonsecurity.auth.TokenAuthService;
import com.novruz.commonsecurity.configuration.SecurityProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TokenAuthConfig {

    @Bean
    public TokenAuthService tokenAuthService(JwtService jwtService) {
        return new TokenAuthService(jwtService);
    }

    @Bean
    public JwtService jwtService(@Autowired SecurityProperties securityProperties) {
        return new JwtService(securityProperties);
    }
}
