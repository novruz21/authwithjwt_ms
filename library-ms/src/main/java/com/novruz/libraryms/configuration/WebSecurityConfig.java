package com.novruz.libraryms.configuration;

import com.novruz.commonsecurity.configuration.BaseSecurityConfig;
import com.novruz.commonsecurity.filter.FilterConfigurerAdapter;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;

import static org.springframework.security.config.Customizer.withDefaults;

@EnableWebSecurity
@ComponentScan("com.novruz.commonsecurity")
public class WebSecurityConfig extends BaseSecurityConfig {

    public WebSecurityConfig(FilterConfigurerAdapter filterConfigurerAdapter) {
        super(filterConfigurerAdapter);
    }

    @Override
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests((authz) -> authz.antMatchers("/library/public").permitAll())
                .httpBasic(withDefaults());
        return super.filterChain(http);
    }

}
